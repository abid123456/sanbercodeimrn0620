// No. 1
function arrayToObject(arr) {
    var objects=[];
    var currentYear = (new Date()).getFullYear();
    for (var i=0; i<arr.length; i++) {
        objects[i]={
            firstName: arr[i][0],
            lastName : arr[i][1],
            gender   : arr[i][2],
            age      : arr[i][3]<currentYear?currentYear-arr[i][3]:-1
        };
        if (objects[i].age<0) objects[i].age="Invalid birth year";
        process.stdout.write(String(i+1)+". "+objects[i].firstName+" "+objects[i].lastName+": ");
        console.log(objects[i]);
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// No. 2
function shoppingTime(memberId="", money) {
    const items=[["Sepatu Staccatu",1500000],["Baju Zoro",500000],["Baju H&N",250000],["Sweater Uniklooh",175000],["Casing Handphone",50000]];
    
    if (memberId=="") return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if (money<50000) return "Mohon maaf, uang tidak cukup";
    var purchased=[];
    var moneyLeft=money;
    for (i=0; i<items.length; i++) {
        if (moneyLeft>=items[i][1]) {
            moneyLeft-=items[i][1];
            purchased.splice(purchased.length,0,items[i][0]);
        }
    }
    var result = {
        memberId     : memberId,
        money        : money,
        listPurchased: purchased,
        changeMoney  : moneyLeft        
    }
    return result;
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// No. 3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var result = [];
  for (var i=0; i<arrPenumpang.length; i++) {
      var cost = 0;
      var started = false;
      for (var j=0; j<rute.length; j++) {
          if (started) cost+=2000;
          if (arrPenumpang[i][1]==rute[j]) started = true;
          if (arrPenumpang[i][2]==rute[j]) break;
      }
      result[i]={
          penumpang: arrPenumpang[i][0],
          naikDari : arrPenumpang[i][1],
          tujuan   : arrPenumpang[i][2],
          bayar    : cost
      }
  }
  return result;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]


































