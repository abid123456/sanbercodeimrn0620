// if-else

var nama = "Flow"
var peran = "Werewolf"

if (nama.length==0) console.log("Nama harus diisi!");
else if (peran.length==0) console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
else {
    var introSentence;
    if (peran == "Penyihir")
        introSentence = "kamu dapat melihat siapa yang menjadi werewolf!";
    else if (peran == "Guard")
        introSentence = "kamu akan membantu melindungi temanmu dari serangan werewolf.";
    else if (peran == "Werewolf")
        introSentence = "Kamu akan memakan mangsa setiap malam!";

    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+", "+introSentence);
}

// switch case

var tanggal = 20;
var bulan   = 10;
var tahun   = 2010;

var stringBulan;
switch (bulan) {
case 1: {stringBulan = "Januari"; break;}
case 2: {stringBulan = "Februari"; break;}
case 3: {stringBulan = "Maret"; break;}
case 4: {stringBulan = "April"; break;}
case 5: {stringBulan = "Mei"; break;}
case 6: {stringBulan = "Juni"; break;}
case 7: {stringBulan = "Juli"; break;}
case 8: {stringBulan = "Agustus"; break;}
case 9: {stringBulan = "September"; break;}
case 10: {stringBulan = "Oktober"; break;}
case 11: {stringBulan = "November"; break;}
case 12: {stringBulan = "Desember"; break;}
}

console.log(tanggal.toString()+" "+stringBulan+" "+tahun.toString());
