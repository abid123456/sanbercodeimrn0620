// no. 1
function range(startNum="", finishNum="") {
    if (startNum==="" || finishNum==="") return -1;
    var step=1;
    if (startNum > finishNum) step *= -1;
    var array=[];
    var i = startNum;
    while ((step>0 && i<=finishNum)||(step<0 && i>=finishNum)) {
        array.push(i);
        i+=step;
    }
    return array;
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log();

// no. 2
function rangeWithStep(startNum="", finishNum="", step) {
    if (startNum==="" || finishNum==="") return -1;
    if (startNum > finishNum) step *= -1;
    var array=[];
    var i = startNum;
    while ((step>0 && i<=finishNum)||(step<0 && i>=finishNum)) {
        array.push(i);
        i+=step;
    }
    return array;
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log();

// no. 3
function sum(startNum, finishNum="", step=1) {
    if (finishNum==="") finishNum=startNum;
    var array = rangeWithStep(startNum, finishNum, step);
    var jumlah = 0;
    for (var i=0; i<array.length; i++) jumlah+=array[i];
    return jumlah;
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

console.log();

// no. 4
function dataHandling(input) {
    for (var i=0; i<input.length; i++) {
        if (i>0) {
            console.log(" ");
        }
        console.log("Nomor ID:  "+input[i][0]);
        console.log("Nama Lengkap:  "+input[i][1]);
        console.log("TTL:  "+input[i][2]+" "+input[i][3]);
        console.log("Hobi:  "+input[i][4]);
    }
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

dataHandling(input);

console.log();

// no. 5
function balikKata(string) {
    var reversed=""
    for (var i=string.length-1; i>=0; i--) {
        reversed+=string[i];
    }
    return reversed;
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log();

// no. 6
function dataHandling2(input) {
    input.splice(1,2,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung");
    input.splice(4,1,"Pria","SMA Internasional Metro");
    console.log(input);
    var date = input[3].split("/");
    var stringBulan;
    switch (Number(date[1])) {
    case 1: {stringBulan = "Januari"; break;}
    case 2: {stringBulan = "Februari"; break;}
    case 3: {stringBulan = "Maret"; break;}
    case 4: {stringBulan = "April"; break;}
    case 5: {stringBulan = "Mei"; break;}
    case 6: {stringBulan = "Juni"; break;}
    case 7: {stringBulan = "Juli"; break;}
    case 8: {stringBulan = "Agustus"; break;}
    case 9: {stringBulan = "September"; break;}
    case 10: {stringBulan = "Oktober"; break;}
    case 11: {stringBulan = "November"; break;}
    case 12: {stringBulan = "Desember"; break;}
    }
    console.log(stringBulan);
    date.sort(function(a,b){return Number(b)-Number(a)});
    console.log(date);
    date = input[3].split("/");
    var joined = date.join("-");
    console.log(joined);
    var sliced = input[1].slice(0,14);
    console.log(sliced);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
