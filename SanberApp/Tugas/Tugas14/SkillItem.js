import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class SkillItem extends Component {
  render() {
    let skill=this.props.skill;
    return (
      <TouchableOpacity style={styles.mainContainer}>
        <View style={{marginLeft:8}}>
          <Icon name={skill.iconName} color="#003366" size={80}/>
        </View>
        <View style={{flex:1,marginLeft:8,marginRight:8}}>
          <Text style={{fontSize:24,lineHeight:28,fontWeight:"bold",color:"#003366"}}>{skill.skillName}</Text>
          <Text style={{fontSize:16,lineHeight:19,fontWeight:"bold",color:"#3EC6FF"}}>{skill.categoryName}</Text>
          <Text style={{fontSize:48,lineHeight:56,fontWeight:"bold",color:"#FFFFFF",alignSelf:"flex-end"}}>{skill.percentageProgress}</Text>
        </View>
        <View>
          <Icon name="chevron-right" color="#003366" size={80}/>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex:1,
    flexDirection:"row",
    width:343,
    height:129,
    borderRadius:8,
    backgroundColor:"#B4E9FF",
    elevation:3,
    margin:4,
    alignItems:"center"
  }
});