import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Ionicon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex:1}}/>
        
        <View style={{flex:1.5}}>
          <Text style={{fontSize:36,color:"#003366",fontWeight:"bold"}}>Tentang Saya</Text>
        </View>
        
        <View style={{flex:5, alignItems: "center"}}>
          <View style={styles.devCircle}>
            <Ionicon name="md-person" color="#CACACA" size={100}/>
          </View>
          <Text style={{fontSize:24,color:"#003366",fontWeight:"bold"}}>Abdillah Ahmad</Text>
          <Text style={{fontSize:16,color:"#3EC6FF",fontWeight:"bold"}}>React Native Developer</Text>
        </View>
        
        <View style={{flex:2,backgroundColor:"#EFEFEF",borderRadius:16,paddingTop:10,paddingLeft:10,paddingRight:10}}>
          <Text style={{fontSize:18,color:"#003366"}}>Portofolio</Text>
          <View style={styles.horizontalLine}/>
          <View style={{flexDirection:"row",justifyContent:"space-around",alignItems:"center"}}>
            <View style={{alignItems:"center"}}>
              <Icon name="gitlab" color="#3EC6FF" size={40}/>
              <Text style={styles.socmedItemText}>@abid123456</Text>
            </View>
            <View style={{alignItems:"center"}}>
              <Ionicon name="logo-github" color="#3EC6FF" size={40}/>
              <Text style={styles.socmedItemText}>@abid123456</Text>
            </View>
          </View>
        </View>
        
        <View style={{flex:0.5}}/>
        
        <View style={{flex:3.5,backgroundColor:"#EFEFEF",borderRadius:16,paddingTop:10,paddingLeft:10,paddingRight:10}}>
          <Text style={{fontSize:18,color:"#003366"}}>Hubungi Saya</Text>
          <View style={styles.horizontalLine}/>
          <View style={{alignItems:"center"}}>
            <View style={{justifyContent:"space-around"}}>
              <View style={styles.socmedItem}>
                <Ionicon name="logo-facebook" color="#3EC6FF" size={40}/>
                <Text style={styles.socmedItemText2}>Abdillah Ahmad</Text>
              </View>
              <View style={styles.socmedItem}>
                <Ionicon name="logo-instagram" color="#3EC6FF" size={40}/>
                <Text style={styles.socmedItemText2}>@thereconnoitrer1716</Text>
              </View>
              <View style={styles.socmedItem}>
                <Ionicon name="logo-twitter" color="#3EC6FF" size={36}/>
                <Text style={styles.socmedItemText2}>@KucingMiaw1</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{flex:0.25}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  socmedItem: {
    flexDirection: "row",
    alignItems:"center"
  },
  socmedItemText: {
    fontSize:16,
    color:"#003366",
    fontWeight:"bold"
  },
  socmedItemText2: {
    fontSize:16,
    color:"#003366",
    fontWeight:"bold",
    marginLeft:18
  },
  devCircle: {
    backgroundColor:"#EFEFEF",
    width:150,
    height:150,
    borderRadius:75,
    alignItems:"center",
    justifyContent:"center"
  },
  horizontalLine: {
    width:320,
    height:0,
    borderColor:"#003366",
    borderWidth:1
  }
});
