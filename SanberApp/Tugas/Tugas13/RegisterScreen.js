import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex:0.5}}/>
        
        <View style={{flex:2.5, justifyContent: "center"}}>
          <Image source={require("./images/logo.png")}/>
        </View>
        
        <View style={{flex:0.5}}>
          <Text style={styles.titleText}>Register</Text>
        </View>
        
        <View style={{flex:7, justifyContent:"center"}}>
          <View style={styles.textEntry}>
            <Text style={styles.boxText}>Username</Text>
            <View style={styles.textBox}></View>
          </View>
          <View style={styles.textEntry}>
            <Text style={styles.boxText}>Email</Text>
            <View style={styles.textBox}></View>
          </View>
          <View style={styles.textEntry}>
            <Text style={styles.boxText}>Password</Text>
            <View style={styles.textBox}></View>
          </View>
          <View style={styles.textEntry}>
            <Text style={styles.boxText}>Ulangi Password</Text>
            <View style={styles.textBox}></View>
          </View>
        </View>
        
        <View style={{flex:2.25, alignItems: "center", justifyContent: "space-around"}}>
          <View style={styles.registerButton}>
            <Text style={styles.buttonText}>Daftar</Text>
          </View>
          <Text style={styles.interButtonText}>
            atau
          </Text>
          <View style={styles.loginButton}>
            <Text style={styles.buttonText}>Masuk ?</Text>
          </View>
        </View>
        
        <View style={{flex:0.5}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  titleText: {
    fontSize: 24,
    color: "#003366"
  },
  buttonText: {
    fontSize: 24,
    color: "#FFFFFF"
  },
  interButtonText: {
    fontSize: 24,
    color: "#3EC6FF"
  },
  boxText: {
    fontSize: 16,
    color: "#003366"
  },
  textBox: {
    borderColor: "#003366",
    borderWidth: 1,
    width: 294,
    height: 48,
    alignItems: "center"
  },
  textEntry: {
    marginTop: 6,
    marginBottom:6
  },
  loginButton: {
    width: 140,
    height: 40,
    borderRadius: 16,
    backgroundColor: "#3EC6FF",
    alignItems: "center",
    justifyContent: "center"
  },
  registerButton: {
    width: 140,
    height: 40,
    borderRadius: 16,
    backgroundColor: "#003366",
    alignItems: "center",
    justifyContent: "center"
  }
});
