import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button
} from 'react-native';

export const Login = ({navigation}) => (
  <View style={styles.container}>
    <View style={{flex:1}}/>
    
    <View style={{flex:4, justifyContent: "center"}}>
      <Image source={require("./images/logo.png")}/>
    </View>
    
    <View style={{flex:1}}>
      <Text style={styles.titleText}>Login</Text>
    </View>
    
    <View style={{flex:5, justifyContent:"center"}}>
      <View style={styles.textEntry}>
        <Text style={styles.boxText}>Username / Email</Text>
        <View style={styles.textBox}></View>
      </View>
      <View style={styles.textEntry}>
        <Text style={styles.boxText}>Password</Text>
        <View style={styles.textBox}></View>
      </View>
    </View>
    
    <View style={{flex:3, alignItems: "center", justifyContent: "space-around"}}>
      <Button
        title="Masuk"
        style={styles.loginButton}
        onPress={() => navigation.push("Drawer")}
      />
      <Text style={styles.interButtonText}>atau</Text>
      <Button
        title="Daftar ?"
        style={styles.registerButton}
      />
    </View>
    
    <View style={{flex:2}}/>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: "center",
    justifyContent: "space-around"
  },
  titleText: {
    fontSize: 24,
    color: "#003366"
  },
  buttonText: {
    fontSize: 24,
    color: "#FFFFFF"
  },
  interButtonText: {
    fontSize: 24,
    color: "#3EC6FF"
  },
  boxText: {
    fontSize: 16,
    color: "#003366"
  },
  textBox: {
    borderColor: "#003366",
    borderWidth: 1,
    width: 294,
    height: 48,
    alignItems: "center"
  },
  textEntry: {
    marginTop: 6,
    marginBottom:6
  },
  loginButton: {
    width: 140,
    height: 40,
    borderRadius: 16,
    backgroundColor: "#3EC6FF",
    alignItems: "center",
    justifyContent: "center"
  },
  registerButton: {
    width: 140,
    height: 40,
    borderRadius: 16,
    backgroundColor: "#003366",
    alignItems: "center",
    justifyContent: "center"
  }
});
