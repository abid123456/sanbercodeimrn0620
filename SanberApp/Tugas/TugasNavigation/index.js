import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import {Login} from "./LoginScreen";
import SkillScreen from "./SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";
import AboutScreen from "./AboutScreen";

const Root = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen
      name="Tabs"
      component={TabsScreen}
      options={()=>({
          title: "Home"
        })}
    />
    <Drawer.Screen
      name="About"
      component={AboutScreen}
      options={()=>({
          title: "About the Developer"
        })}
    />
  </Drawer.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skills" component={SkillScreen} />
    <Tabs.Screen name="Projects" component={ProjectScreen} />
    <Tabs.Screen name="Add" component={AddScreen} />
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <Root.Navigator>
      <Root.Screen name="Login" component={Login}/>
      <Root.Screen
        name="Drawer"
        component={DrawerScreen}
        options={()=>({
          title: "Sanbercode Portofolio"
        })}
      />
    </Root.Navigator>
  </NavigationContainer>
);
