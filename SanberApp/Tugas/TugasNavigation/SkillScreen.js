import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkillItem from './SkillItem';
import data from './skillData.json';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        
        <View style={{flex:1,justifyContent:"space-around"}}>
          <View style={{alignItems:"flex-end"}}>
            <View style={{marginTop:8}}>
              <Image source={require("./images/logo.png")} style={{width:187.5,height:51}}/>
            </View>
          </View>
          
          <View style={{flexDirection:"row",alignItems:"center",paddingLeft:16,paddingRight:16}}>
            <Icon name="account-circle" size={32} color="#3EC6FF"/>
            <View style={{marginLeft:8}}>
              <Text style={{fontSize:12, lineHeight:14}}>Hai,</Text>
              <Text style={{fontSize:16, lineHeight:19, color:"#003366"}}>Mukhlis Hanafi</Text>
            </View>
          </View>
          
          <View style={{alignItems:"center"}}>
            <View style={{alignItems:"flex-start"}}>
              <Text style={{fontSize:36,lineHeight:42,color:"#003366"}}>SKILL</Text>
              <View style={{width:343,height:4,backgroundColor:'#3EC6FF'}}/>
            </View>
          </View>
        </View>
        
        <View style={{flex:0.25,flexDirection:"row",justifyContent:"space-around"}}>
          <TouchableOpacity style={styles.categoryItem}>
            <Text style={styles.categoryItemText}>Library / Framework</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.categoryItem}>
            <Text style={styles.categoryItemText}>Bahasa Pemrograman</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.categoryItem}>
            <Text style={styles.categoryItemText}>Teknologi</Text>
          </TouchableOpacity>
        </View>
        
        <View style={{flex:2.5,alignItems:"center"}}>
          <FlatList 
          data={data.items}
          renderItem={(skill)=><SkillItem skill={skill.item}/>}
          keyExtractor={(item)=>item.id.toString()}
          />
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: "space-around"
  },
  categoryItem: {
    alignItems:"center",
    justifyContent:"center",
    height:32,
    borderRadius:8,
    backgroundColor:"#B4E9FF",
    padding:8
  },
  categoryItemText: {
    fontSize:12,
    lineHeight:14,
    fontWeight:"bold",
    color:"#003366"
  }
});
