// Tugas 9 - ES6

// No. 1
/* Original:
const golden = function goldenFunction(){
  console.log("this is golden!!")
}
 
golden()
*/
const golden = ()=>{
  console.log("this is golden!!")
}
 
golden()


// No. 2
/* Original:
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
*/
const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}

newFunction("William", "Imoh").fullName() 


// No. 3
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",  spell: "Vimulus Renderus!!!"
}
/* Original:
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
*/
const {firstName,lastName,destination,occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

// No. 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
/* Original:
const combined = west.concat(east)
*/
const combined = [...west, ...east];
console.log(combined)

// No. 5
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
	
var after = `Lorem ${view}dolor sit amet, ` +  
    `consectetur adipiscing elit,${planet}do eiusmod tempor ` +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
before = after;
console.log(before) 



































