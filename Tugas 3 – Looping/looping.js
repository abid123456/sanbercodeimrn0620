// no. 1
console.log("LOOPING PERTAMA");
var i=2;
while (i<=20) {
    console.log(i.toString()+" - I love coding");
    i += 2;
}
console.log("LOOPING KEDUA");
i=20;
while (i>0) {
    console.log(i.toString()+" - I will become a mobile developer");
    i -= 2;
}

console.log();

// no. 2
for (var j=1; j<=20; j++) {
    var str;
    if (j%2==1) {
        if (j%3==0) str = "I Love Coding";
        else str = "Santai";
    } else str = "Berkualitas";
    console.log(j.toString()+" - "+str);
}

console.log();

// no. 3
var baris="";
for (var x=0; x<8; x++) baris+="#";
for (var y=0; y<4; y++) console.log(baris);

console.log();

// no. 4
var line_now="";
for (var n=0; n<7; n++) {
    line_now += "#";
    console.log(line_now);
}

console.log();

// no. 5

var line;
for (var y=0; y<8; y++) {
    line="";
    for (var x=0; x<8; x++) {
        if ((x+y)%2==1) line += "#";
        else line += " ";
    }
    console.log(line);
}
