// No. 1
class Animal {
    constructor(x) {
        this.name = x;
        this.legs = 4;
        this.cold_blooded = false;
    }
    get name() {return this._name;}
    set name(x) {this._name = x;}
    get legs() {return this._legs;}
    set legs(x) {this._legs = x;}
    get cold_blooded() {return this._cold_blooded;}
    set cold_blooded(x) {this._cold_blooded = x;}
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

class Ape extends Animal {
    constructor(x) {
        super(x);
        this.legs = 2;
    }
    yell() {
         console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(x) {
        super(x);
        this.cold_blooded = true;
    }
    jump() {
        console.log("hop hop");
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell()
 
var kodok = new Frog("buduk")
kodok.jump()

// No. 2
class Clock {
    constructor({template}) {
        this.template = template;
    }
    
    get date() {
        return new Date();
    }
    get hours() {
        var result = this.date.getHours();
        if (result<10) result='0'+result;
        return result;
    }
    get mins() {
        var result = this.date.getMinutes();
        if (result<10) result='0'+result;
        return result;
    }
    get secs() {
        var result = this.date.getSeconds();
        if (result<10) result='0'+result;
        return result;
    }
    get output() {
        var result = this.template
                .replace('h', this.hours)
                .replace('m', this.mins)
                .replace('s', this.secs);
        return result;
    }
    
    render() {
        console.log(this.output);
    }
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
    stop() {
        clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();

