var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan
// function readBooksPromise

var timeLeft=10000;
var timeUp = false;
function readBook(number) {
	if (number >= books.length || timeUp) return;
	readBooksPromise(timeLeft, books[number])
	    .then(function(value) {
			timeLeft=value;
			readBook(number+1);
		})
		.catch(function(error) {
			timeUp = true;
		});
}
readBook(0);